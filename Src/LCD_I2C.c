/*
 * LCD_I2C.c
 *
 *  Created on: 26 апр. 2018 г.
 *      Author: sergey
 */

#include "LCD_I2C.h"

//extern I2C_HandleTypeDef hi2c2;

uint8_t buf[1] = {0};

uint8_t LCD_WriteByteI2C(uint8_t byte)
{
	buf[0] = byte;
	return I2C_Soft_WriteBuffer(&I2C_SoftLCD, 0x4E, buf, 1);

//	return I2C_Master_Transmit(I2C2, 0x4E, buf, 1);
}
void LCD_Send_HalfByte(uint8_t c)
{
	c <<= 4;
	e_set();
	DWT_Delay_us(50);
	LCD_WriteByteI2C(portlcd | c);
	e_reset();
	DWT_Delay_us(50);
}
void LCD_Send_Byte(uint8_t c, uint8_t mode)
{
	if (mode == 0) rs_reset();
	else rs_set();
	uint8_t hc = 0;
	hc = c >> 4;
	LCD_Send_HalfByte(hc);
	LCD_Send_HalfByte(c);
}
void LCD_Clear(void)
{
	LCD_Send_Byte(0x01, 0);
	DWT_Delay_us(2000);
}
void LCD_Send_Char(char ch)
{
	LCD_Send_Byte(ch, 1);
}
void LCD_SetPos(uint8_t x, uint8_t y)
{
	switch(y)
	{
	case 0:
		LCD_Send_Byte(x|0x80, 0);
		DWT_Delay_us(1000);
		break;
	case 1:
		LCD_Send_Byte((0x40 + x) | 0x80, 0);
		DWT_Delay_us(1000);
		break;
	default:
		LCD_Send_Byte(x|0x80, 0);
		DWT_Delay_us(1000);
		break;
	}
}
void LCD_I2C_Init(void)
{
	I2C_SoftLCD.Port_Periph = LL_APB2_GRP1_PERIPH_GPIOB;
	I2C_SoftLCD.SCL_GPIO_PIN = LL_GPIO_PIN_10;
	I2C_SoftLCD.SCL_GPIO_PORT = GPIOB;
	I2C_SoftLCD.SDA_GPIO_PIN = LL_GPIO_PIN_11;
	I2C_SoftLCD.SDA_GPIO_PORT = GPIOB;
	I2C_SoftLCD.I2C_Delay_us = 2;
	I2C_Soft_Init(&I2C_SoftLCD);

	portlcd = 0x00;
	DWT_Delay_us(15000);
	LCD_Send_HalfByte(0x03);
	DWT_Delay_us(4000);
	LCD_Send_HalfByte(0x03);
	DWT_Delay_us(100);
	LCD_Send_HalfByte(0x03);
	DWT_Delay_us(1000);

	LCD_Send_HalfByte(0x02);
	DWT_Delay_us(1000);

	LCD_Send_Byte(0x28, 0);
	DWT_Delay_us(1000);

	LCD_Send_Byte(0x0C, 0);
	DWT_Delay_us(1000);

	LCD_Send_Byte(0x01, 0);
	DWT_Delay_us(2000);

	LCD_Send_Byte(0x06, 0);
	DWT_Delay_us(1000);

	led_set();
	write_set();
}

void LCD_Send_Str(char* str)
{
	uint8_t i = 0;
	while(str[i] != 0)
	{
		LCD_Send_Byte(str[i], 1);
		i++;
	}
}

