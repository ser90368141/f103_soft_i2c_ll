/*
 * LCD_I2C.h
 *
 *  Created on: 26 апр. 2018 г.
 *      Author: sergey
 */

#ifndef LCD_I2C_H_
#define LCD_I2C_H_

//---------------
#include <I2C_Soft_LL.h>

#include <stdio.h>
#include "stdlib.h"

#include "DWT_Delay.h"
//---------------
#define e_set() LCD_WriteByteI2C(portlcd|=0x04)
#define e_reset() LCD_WriteByteI2C(portlcd&=~0x04)
#define rs_set() LCD_WriteByteI2C(portlcd|=0x01)
#define rs_reset() LCD_WriteByteI2C(portlcd&=~0x01)
#define led_set() LCD_WriteByteI2C(portlcd|=0x08)
#define led_reset() LCD_WriteByteI2C(portlcd&=~0x08)
#define read_set() LCD_WriteByteI2C(portlcd|=0x02)
#define write_set() LCD_WriteByteI2C(portlcd&=~0x02)
//---------------
#define SUCSESS 1;
#define ERROR 0
//---------------
uint8_t portlcd;
//---------------
I2C_Soft_TypeDef I2C_SoftLCD;
//---------------
uint8_t LCD_WriteByteI2C(uint8_t byte);
void LCD_Send_HalfByte(uint8_t);
void LCD_Send_Byte(uint8_t, uint8_t);
void LCD_Clear(void);
void LCD_Send_Char(char ch);
void LCD_SetPos(uint8_t x, uint8_t y);
void LCD_I2C_Init(void);
void LCD_Send_Str(char* str);

#endif /* LCD_I2C_H_ */
