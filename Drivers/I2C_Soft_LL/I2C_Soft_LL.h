/*
 * I2C_Sort_LL.h
 *
 *  Created on: 23 окт. 2018 г.
 *      Author: sergey
 */

#ifndef I2C_SOFT_LL_I2C_SOFT_LL_H_
#define I2C_SOFT_LL_I2C_SOFT_LL_H_

#include "stdbool.h"

#include "stm32f1xx.h"
#include "stm32f1xx_ll_system.h"
#include "stm32f1xx_ll_gpio.h"
#include "stm32f1xx_ll_exti.h"
#include "stm32f1xx_ll_bus.h"

#include "main.h"

#include "DWT_Delay.h"

#define I2C_RESULT_SUCCESS          0
#define I2C_RESULT_ERROR            (-1)

typedef struct
{
	uint32_t Port_Periph;
	GPIO_TypeDef *SDA_GPIO_PORT;
	uint32_t SDA_GPIO_PIN;
	GPIO_TypeDef *SCL_GPIO_PORT;
	uint32_t SCL_GPIO_PIN;
	uint16_t I2C_Delay_us;
} I2C_Soft_TypeDef;

bool I2C_Soft_Init(I2C_Soft_TypeDef *I2C_SOFTx);
int I2C_Soft_ReadBuffer(I2C_Soft_TypeDef *I2C_SOFTx, uint8_t chipAddress, uint8_t *buffer, uint32_t sizeOfBuffer);
int I2C_Soft_WriteBuffer(I2C_Soft_TypeDef *I2C_SOFTx, uint8_t chipAddress, uint8_t *buffer, uint32_t sizeOfBuffer);

//bool I2C_Soft_Send(uint8_t byte_in);


#endif /* I2C_SOFT_LL_I2C_SOFT_LL_H_ */
