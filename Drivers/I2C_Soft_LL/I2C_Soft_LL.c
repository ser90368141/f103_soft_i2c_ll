/*
 * I2C_Soft_LL.c
 *
 *  Created on: 23 окт. 2018 г.
 *      Author: sergey
 */

#include <I2C_Soft_LL.h>

void SCLH(I2C_Soft_TypeDef *I2C_SOFTx)
{
	LL_GPIO_SetOutputPin(I2C_SOFTx->SCL_GPIO_PORT, I2C_SOFTx->SCL_GPIO_PIN);
}
void SCLL(I2C_Soft_TypeDef *I2C_SOFTx)
{
	LL_GPIO_ResetOutputPin(I2C_SOFTx->SCL_GPIO_PORT, I2C_SOFTx->SCL_GPIO_PIN);
}
void SDAH(I2C_Soft_TypeDef *I2C_SOFTx)
{
	LL_GPIO_SetOutputPin(I2C_SOFTx->SDA_GPIO_PORT, I2C_SOFTx->SDA_GPIO_PIN);
}
void SDAL(I2C_Soft_TypeDef *I2C_SOFTx)
{
	LL_GPIO_ResetOutputPin(I2C_SOFTx->SDA_GPIO_PORT, I2C_SOFTx->SDA_GPIO_PIN);
}
uint32_t SCLread(I2C_Soft_TypeDef *I2C_SOFTx)
{
	return LL_GPIO_IsInputPinSet(I2C_SOFTx->SCL_GPIO_PORT, I2C_SOFTx->SCL_GPIO_PIN);
}
uint32_t SDAread(I2C_Soft_TypeDef *I2C_SOFTx)
{
	return LL_GPIO_IsInputPinSet(I2C_SOFTx->SDA_GPIO_PORT, I2C_SOFTx->SDA_GPIO_PIN);
}


//bool I2C_Soft_Init(uint32_t PORT_PERIPH, GPIO_TypeDef *SDA_GPIO_PORT, uint32_t SDA_GPIO_PIN,
//		GPIO_TypeDef *SCL_GPIO_PORT, uint32_t SCL_GPIO_PIN)
bool I2C_Soft_Init(I2C_Soft_TypeDef *I2C_SOFTx)
{
	LL_GPIO_InitTypeDef GPIO_InitStruct;

	LL_APB2_GRP1_EnableClock(I2C_SOFTx->Port_Periph);

	LL_GPIO_SetOutputPin(I2C_SOFTx->SDA_GPIO_PORT, I2C_SOFTx->SDA_GPIO_PIN);
	LL_GPIO_SetOutputPin(I2C_SOFTx->SCL_GPIO_PORT, I2C_SOFTx->SCL_GPIO_PIN);

	/**/
	GPIO_InitStruct.Pin = I2C_SOFTx->SDA_GPIO_PIN;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
	LL_GPIO_Init(I2C_SOFTx->SDA_GPIO_PORT, &GPIO_InitStruct);

	/**/
	GPIO_InitStruct.Pin = I2C_SOFTx->SCL_GPIO_PIN;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
	LL_GPIO_Init(I2C_SOFTx->SCL_GPIO_PORT, &GPIO_InitStruct);

	return true;
}

/**
 *  @brief  Отправка последовательности СТАРТ в шину
 *  @param  void
 *  @return bool - результат выполнения функции:
 *          true в случае успеха
 *          false в случае ошибки
 */
bool I2C_Soft_Start(I2C_Soft_TypeDef *I2C_SOFTx)
{
    SDAH(I2C_SOFTx);                       // отпустить обе линии, на случай
    SCLH(I2C_SOFTx);                       // на случай, если они были прижаты
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    if ( !(SDAread(I2C_SOFTx)) )           // если линия SDA прижата слейвом,
        return false;           // то сформировать старт невозможно, выход с ошибкой
    SDAL(I2C_SOFTx);                       // прижимаем SDA к земле
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    if ( SDAread(I2C_SOFTx) )              // если не прижалась, то шина неисправна
        return false;           // выход с ошибкой
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    return true;                // старт успешно сформирован
}

/**
 *  @brief  Отправка последовательности СТОП в шину
 *  @param  void
 *  @return bool - результат выполнения функции:
 *          true в случае успеха
 *          false в случае ошибки
 */
bool I2C_Soft_Stop(I2C_Soft_TypeDef *I2C_SOFTx)
{
    SCLL(I2C_SOFTx);                       // последовательность для формирования Стопа
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SDAL(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SCLH(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SDAH(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    return true;
}

/**
 *  @brief  Отправка последовательности ACK в шину
 *  @param  void
 *  @return void
 */
void I2C_Soft_ACK(I2C_Soft_TypeDef *I2C_SOFTx)
{
    SCLL(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SDAL(I2C_SOFTx);                       // прижимаем линию SDA к земле
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SCLH(I2C_SOFTx);                       // и делаем один клик линием SCL
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SCLL(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
}

/**
 *  @brief  Отправка последовательности NO ACK в шину
 *  @param  void
 *  @return void
 */
void I2C_Soft_NACK(I2C_Soft_TypeDef *I2C_SOFTx)
{
    SCLL(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SDAH(I2C_SOFTx);                       // отпускаем линию SDA
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SCLH(I2C_SOFTx);                       // и делаем один клик линием SCL
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SCLL(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
}

/**
 *  @brief  Проверка шины на наличие ACK от слейва
 *  @param  void
 *  @return bool - результат выполнения функции:
 *          true  - если ACK получен
 *          false - если ACK НЕ получен
 */
bool I2C_Soft_WaitAck(I2C_Soft_TypeDef *I2C_SOFTx)
{
    SCLL(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SDAH(I2C_SOFTx);
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    SCLH(I2C_SOFTx);                       // делаем половину клика линией SCL
    DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
    if ( SDAread(I2C_SOFTx) ) {            // и проверяем, прижал ли слейв линию SDA
        SCLL(I2C_SOFTx);
        return false;
    }
    SCLL(I2C_SOFTx);                       // завершаем клик линией SCL
    return true;
}

/**
 *  @brief  Отправка одного байта data в шину
 *  @param  uint8_t data - байт данных для отправки
 *  @return void
 */
void I2C_Soft_PutByte(I2C_Soft_TypeDef *I2C_SOFTx, uint8_t data)
{
    uint8_t i = 8;              // нужно отправить 8 бит данных
    while ( i-- )
    {             // пока не отправили все биты
        SCLL(I2C_SOFTx);                   // прижимаем линию SCL к земле
        DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
        if ( data & 0x80 )      // и выставляем на линии SDA нужный уровень
            SDAH(I2C_SOFTx);
        else
            SDAL(I2C_SOFTx);
        data <<= 1;
        DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
        SCLH(I2C_SOFTx);                   // отпускаем линию SCL
        DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);        // после этого слейв сразу же прочитает значение на линии SDA
    }
    SCLL(I2C_SOFTx);
}

/**
 *  @brief  Чтение одного байта data из шины
 *  @param  void
 *  @return uint8_t - прочитанный байт
 */
uint8_t I2C_Soft_GetByte(I2C_Soft_TypeDef *I2C_SOFTx)
{
    volatile uint8_t i = 8;     // нужно отправить 8 бит данных
    uint8_t data = 0;

    SDAH(I2C_SOFTx);                       // отпускаем линию SDA. управлять ей будет слейв
    while ( i-- ) {             // пока не получили все биты
        data <<= 1;
        SCLL(I2C_SOFTx);                   // делаем клик линией SCL
        DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
        SCLH(I2C_SOFTx);
        DWT_Delay_us(I2C_SOFTx->I2C_Delay_us);
        if ( SDAread(I2C_SOFTx) ) {        // читаем значение на линии SDA
            data |= 0x01;
        }
    }
    SCLL(I2C_SOFTx);
    return data;                // возвращаем прочитанное значение
}

/**
 *  @brief  Чтение данных из шины в буфер buffer, размером sizeOfBuffer
 *          у слейва с адресом chipAddress.
 *  @param  uint8_t chipAddress     - адрес подчиненного
 *          uint8_t *buffer         - указатель на буфер, куда класть
 *                                    прочитанные данные
 *          uint32_t sizeOfBuffer   - количество байт для чтения
 *  @return int - результат выполнения фунции:
 *          I2C_RESULT_SUCCESS  - в случае успеха
 *          I2C_RESULT_ERROR    - в случае ошибки
 */
int I2C_Soft_ReadBuffer(I2C_Soft_TypeDef *I2C_SOFTx, uint8_t chipAddress, uint8_t *buffer, uint32_t sizeOfBuffer )
{
	if ( !I2C_Soft_Start(I2C_SOFTx) )
		return I2C_RESULT_ERROR;

	I2C_Soft_PutByte(I2C_SOFTx, chipAddress + 1 );
	if ( !I2C_Soft_WaitAck(I2C_SOFTx) ) {
		I2C_Soft_Stop(I2C_SOFTx);
		return I2C_RESULT_ERROR;
	}

	while ( sizeOfBuffer != 0 ) {
		*buffer = I2C_Soft_GetByte(I2C_SOFTx);

		buffer++;
		sizeOfBuffer--;
		if ( sizeOfBuffer == 0 ) {
			I2C_Soft_NACK(I2C_SOFTx);
			break;
		}
		else
			I2C_Soft_ACK(I2C_SOFTx);
	}
	I2C_Soft_Stop(I2C_SOFTx);
	return I2C_RESULT_SUCCESS;
}

/**
 *  @brief  Запись данных в шину из буфера buffer, размером sizeOfBuffer
 *          в слейва с адресом chipAddress.
 *  @param  uint8_t chipAddress     - адрес подчиненного
 *          uint8_t *buffer         - указатель на буфер, откуда читать
 *                                    записываемые данные
 *          uint32_t sizeOfBuffer   - количество байт для записи
 *  @return int - результат выполнения фунции:
 *          I2C_RESULT_SUCCESS  - в случае успеха
 *          I2C_RESULT_ERROR    - в случае ошибки
 */
int I2C_Soft_WriteBuffer(I2C_Soft_TypeDef *I2C_SOFTx, uint8_t chipAddress, uint8_t *buffer, uint32_t sizeOfBuffer )
{
    if ( !I2C_Soft_Start(I2C_SOFTx) )
        return I2C_RESULT_ERROR;

    I2C_Soft_PutByte(I2C_SOFTx, chipAddress );
    if ( !I2C_Soft_WaitAck(I2C_SOFTx) ) {
    	I2C_Soft_Stop(I2C_SOFTx);
        return I2C_RESULT_ERROR;
    }

    while ( sizeOfBuffer != 0 ) {
    	I2C_Soft_PutByte(I2C_SOFTx, *buffer );
        if ( !I2C_Soft_WaitAck(I2C_SOFTx) ) {
        	I2C_Soft_Stop(I2C_SOFTx);
            return I2C_RESULT_ERROR;
        }

        buffer++;
        sizeOfBuffer--;
    }
    I2C_Soft_Stop(I2C_SOFTx);
    return I2C_RESULT_SUCCESS;
}
